#include<mpi.h>
#include <stdio.h>

int main() {
	
	int first = 1,second = 2 ,third = 3,fourth = 4;
	
	MPI_Init(NULL,NULL);
	
	int rank,size;
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	if(rank == 0) 
		MPI_Send(&first,1,MPI_INT,1,0,MPI_COMM_WORLD);
	
	if(rank == 1) {

		MPI_Recv(&first,1,MPI_INT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		
		printf("process rank-  %d , data received %d \n",rank,first);
		
		MPI_Send(&second,1,MPI_INT,2,12,MPI_COMM_WORLD);
	}
	
	if(rank == 2) {
		MPI_Recv(&second,1,MPI_INT,1,12,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		
		printf("process rank-  %d , data received %d \n",rank,second);
		MPI_Send(&third,1,MPI_INT,3,13,MPI_COMM_WORLD);
	}
	
	if(rank == 3) {
		MPI_Recv(&third,1,MPI_INT,2,13,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		
		printf("process rank-  %d , data received %d \n",rank,third);
		
		MPI_Send(&fourth,1,MPI_INT,0,14,MPI_COMM_WORLD);
	}
	
	if(rank == 0) {
		MPI_Recv(&fourth,1,MPI_INT,3,14,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		
		printf("process rank-  %d , data received %d \n",rank,fourth);
	}
	
	MPI_Finalize();
	
}
