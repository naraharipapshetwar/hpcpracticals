#include<stdio.h>
#include<mpi.h>


int main() {

		MPI_Init(NULL,NULL);

    int rank,size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    
    MPI_Request send_request,recv_request;
    MPI_Status status;
    
    int curr_process = rank % size;
		int next_process = (rank+1) % size;
		
		int num = 0;
		MPI_Isend(&num,1,MPI_INT,curr_process,0,MPI_COMM_WORLD,&send_request);
		printf("sender_process: %d sender_data: %d \n",curr_process,num);

		int k;
		MPI_Irecv(&k,1,MPI_INT,next_process,0,MPI_COMM_WORLD,&recv_request);
		printf("receiver_process: %d received_data: %d \n",next_process,k);
		  	
	  MPI_Finalize();
	  return 0;

}    
