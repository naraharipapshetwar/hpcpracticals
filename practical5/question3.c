#include <mpi.h> 
#include <stdio.h> 
#include <stdlib.h>
 
int main(int argc, char* argv[])
{ 
	 MPI_Init(NULL,NULL); 
		
	 int size, rank; 
	 MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
	 MPI_Comm_size(MPI_COMM_WORLD, &size); 
	 
	 
	 int partial_sum = 0; 
	 
	 if (rank == 0) 
	 { 
			int a[] = { 324,32,5,67,1}; 
			for(int i = 0; i < 5; i++) 
				partial_sum += a[i]; 
	 }
	 else if (rank == 1)
	 { 
			int a[] = {43,56,7,32,4}; 
			for(int i = 0; i < 5; i++) 
				partial_sum += a[i]; 
	 }  
	 
	 int sum; 
	 MPI_Reduce(&partial_sum, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	 
	 if (rank == 0)
	 	printf("Sum of array is : %d\n", sum); 
	 
	 MPI_Finalize(); 
	 return 0; 
}
