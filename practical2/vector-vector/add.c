#include <omp.h>
#include <stdio.h>

int main() {

	int n;
//	scanf("%d",&n);
	
	omp_set_num_threads(4);
	n = 1000;
	int a[n],b[n],c[n];
	
//	for(int i=0;i<n;i++) scanf("%d",&a[i]);
//	for(int i=0;i<n;i++) scanf("%d",&b[i]);

	#pragma omp parallel 
	{
		#pragma omp for nowait
		for(int i=0;i<1000;i++)
			a[i] = i;
		
		#pragma omp for nowait
		for(int i =0;i<1000;i++)
			b[i] = i+2;
	}
	
	#pragma omp parallel for default(shared)
		for(int i=0;i<n;i++)
			c[i] = a[i] + b[i];
	
	for(int i=0;i<n;i++)
		printf("%d ",c[i]);
}
